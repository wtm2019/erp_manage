<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('com_attr_type')->nullable(true)->default('0')->comment('企业属性，1-工厂端，2-销售端');;
            $table->integer('com_type')->nullable(true)->default('0')->comment('企业类型');
            $table->tinyInteger('member_type')->nullable(true)->default('1')->comment('会员等级');
            $table->string('com_code', 30)->nullable(true)->default('')->comment('企业代码');
            $table->string('com_name', 100)->nullable(true)->default('')->comment('企业名称');
            $table->string('com_credit_code', 50)->nullable(true)->default('')->comment('企业信用代码');
            $table->string('com_login_name', 20)->nullable(true)->default('')->comment('企业登陆名称');
            $table->string('pwd', 65)->nullable(true)->default('')->comment('密码');
            $table->string('com_link_man', 20)->nullable(true)->default('')->comment('联系人');
            $table->string('mobile_phone', 20)->nullable(true)->default('')->comment('手机号码');
            $table->string('province', 20)->nullable(true)->default('')->comment('省份');
            $table->string('city', 20)->nullable(true)->default('')->comment('市级');
            $table->string('county', 20)->nullable(true)->default('')->comment('区/县');
            $table->string('db_name', 50)->nullable(true)->default('')->comment('数据库名');
            $table->text('address')->nullable(true)->comment('详细地址');
            $table->text('note')->nullable(true)->comment('备注');
            $table->timestamps();
            $table->timestamp('enddate_at')->nullable(true);
            $table->timestamp('last_login_date_at')->nullable(true);
            $table->tinyInteger('status')->nullable(true)->default('1')->comment('状态 1-禁用,2-启用,3-已过期');
            $table->index('com_name', 'com_name_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_customers');
    }
}
