<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCategorys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 企业类型
        Schema::create('company_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('com_attr_type')->nullable(true)->default('0')->comment('企业属性，1-工厂端，2-销售端');
            $table->string('com_type_name', 100)->nullable(true)->default('')->comment('企业类型名称');
            $table->text('menu_auth')->nullable(true)->comment('菜单权限');
            $table->tinyInteger('status')->nullable(true)->default('1')->comment('状态 1-禁用,2-启用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_categories');
    }
}
