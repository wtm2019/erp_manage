<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name', 100)->nullable(true)->default('')->comment('用户名');
            $table->string('user_login_name', 20)->nullable(true)->default('')->comment('用户登陆名称');
            $table->string('pwd', 65)->nullable(true)->default('')->comment('密码');
            $table->tinyInteger('status')->nullable(true)->default('2')->comment('状态 1-禁用,2-启用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
