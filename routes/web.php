<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//发送短信验证码的接口
Route::get('api/SendMsg/sendVerificationCode', "Api\SendMsgController@sendVerificationCode");
//注册的接口
Route::post('Auth/Register/create', 'Auth\RegisterController@create');

//获取登录图片验证码
Route::get('Auth/Login/getCode', 'Auth\LoginController@getCode');
//登入
Route::post('Auth/Login/login', 'Auth\LoginController@login');
//登出
Route::post('Auth/Logout/logout', 'Auth\LoginController@logout');


//检查公司是否注册，供客户端调用
Route::get('Api/CompanyCustomer/CheckCompany', 'Api\CompanyCustomerController@CheckCompany');
//更新企业用户登陆的时间的接口
Route::get('Api/CompanyCustomer/updateLastLogin', 'Api\CompanyCustomerController@updateLastLogin');



Route::group(['middleware' => ['checkLogin']], function() {
    //添加用户的接口
    Route::post('System/User/createIndex', 'System\UserController@createIndex');
    //获取用户列表的接口
    Route::get('System/User/getIndex', 'System\UserController@getIndex');
    //编辑用户的接口
    Route::post('System/User/editIndex', 'System\UserController@editIndex');
    //启用/禁用用户的接口
    Route::post('System/User/changeStatus', 'System\UserController@changeStatus');

        //添加企业类型的接口
    Route::post('Member/CompanyCategory/createIndex', 'Member\CompanyCategoryController@createIndex');
    //获取企业类型的接口
    Route::get('Member/CompanyCategory/getIndex', 'Member\CompanyCategoryController@getIndex');
    //获取所有启用状态的企业类型的接口
    Route::get('Member/CompanyCategory/getEnabledIndex', 'Member\CompanyCategoryController@getEnabledIndex');
    //编辑企业类型的接口
    Route::post('Member/CompanyCategory/editIndex', 'Member\CompanyCategoryController@editIndex');
    //启用/禁用企业类型的接口
    Route::post('Member/CompanyCategory/changeStatus', 'Member\CompanyCategoryController@changeStatus');


    //添加会员的接口
    Route::post('Member/CompanyCustomer/createIndex', 'Member\CompanyCustomerController@createIndex');
    //获取企业类型的接口
    Route::get('Member/CompanyCustomer/getIndex', 'Member\CompanyCustomerController@getIndex');
    //编辑企业类型的接口
    Route::post('Member/CompanyCustomer/editIndex', 'Member\CompanyCustomerController@editIndex');
    //启用/禁用企业类型的接口
    Route::post('Member/CompanyCustomer/changeStatus', 'Member\CompanyCustomerController@changeStatus');

});