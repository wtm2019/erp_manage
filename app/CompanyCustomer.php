<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CompanyCustomer extends Model
{
    private static $tbCompanyCustomer = 'company_customers';
    private static $tbCompanyCategory = 'company_categories';
    //
    protected $fillable = [
        'com_name', 'com_credit_code', 'com_link_man',
        'mobile_phone', 'pwd', 'com_attr_type',
        'com_code', 'com_login_name', 'address',
        'note', 'enddate_at', 'status', 'com_type',
        'province', 'city', 'county', 'last_login_date_at'
    ];

    public static function getCompanyCustomer($where, $limit, $offset) {
        $companyCustomerList = DB::table(self::$tbCompanyCustomer . ' as customer')
            ->select(
                'customer.id', 'customer.com_attr_type', 'customer.com_type', 'customer.com_code',
                'customer.com_name', 'customer.com_login_name', 'customer.com_link_man', 'customer.address',
                'customer.mobile_phone', 'customer.status', 'customer.created_at', 'customer.enddate_at',
                'category.com_type_name', 'customer.last_login_date_at', 'customer.province', 'customer.city',
                'customer.com_credit_code', 'customer.note'
            )
            ->leftjoin(self::$tbCompanyCategory . ' as category', 'category.id', '=', 'customer.com_type')
            ->where($where)
            ->orderBy('created_at', 'desc')
            ->limit($limit)->offset($offset)
            ->get()
            ->toArray();

        return $companyCustomerList;
    }

    public static function getTotal($where) {
        return DB::table(self::$tbCompanyCustomer . ' as customer')
            ->where($where)
            ->count();
    }
}
