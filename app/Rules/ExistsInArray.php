<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ExistsInArray implements Rule
{
    private $arr = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($arr)
    {
        //
        $this->arr = $arr;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        return in_array($value, $this->arr);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :field must be exist in the array: ' . json_encode($this->arr);
    }
}
