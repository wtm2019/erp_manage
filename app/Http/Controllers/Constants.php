<?php
/**
 * Created by PhpStorm.
 * User: charlie
 * Date: 2019/1/4
 * Time: 1:07 PM
 */

namespace App\Http\Controllers;

class Constants
{
    const TEMPLATE_CODE ='SMS_139970365';
    const LOGIN_REDIS_EXPIRE = 60 * 60 * 4;
    const DB_NAME = 'erp_customer';
//    define("LoginCookieExpire",time()+3600*24; //4 hours
//    define("LoginRedisExpire",60*60*4; //4 hours
//    const sqlFile =__DIR__ . '/../sql/mysql_init.sql';
//    const sqlFileData =__DIR__ . '/../sql/mysql_init_data.sql';
//    const Env =__DIR__ . '/../.env';
//    const db_host ='120.79.240.184';
//    const db_username ='root';
//    const db_password ='root@123';
//    const db_root ='platform_register';
//    const CommonHost ='localhost:8085/';
//    const getCommonApi ='api/User/gextIndex';
//    const IfMakePrice =2;
//    const skuStock_import =1;
//    const skuStock_export =2;
//    const SKU_STOCK_INIT =3; // 库存初始化
//    const IS_SUPER =1;
//    const NOT_SUPER =0;
//    const ACCOUNT_TYPE_CUSTOMER =1;
//    const ACCOUNT_TYPE_SUPPLIER =2;
//    const INIT_LIMIT = 500; // 初始化的限制条数

//    const ERROR_REQUEST_ERROR = 100;
//    const ERROR_PARAM_ERROR = 200;
//    const ERROR_NETWORK_ERROR = 400;
//    const ERROR_SERVER_ERROR = 500;

//    $is_debug = true;
//    if ($is_debug){ // 测试
//    const DEBUG_CURL_HOST = 'http://120.79.240.184:8086/';
//    const DB_HOST = '120.79.240.184';
//    const DB_USERNAME = 'root';
//    const DB_PASSWORD = 'root@123';
//    } else { // 线上
//        const DEBUG_CURL_HOST = 'http://119.23.220.177:8084/';
//        const DB_HOST = 'rm-wz93aileb10q16519.mysql.rds.aliyuncs.com';
//        const DB_USERNAME = 'erproot';
//        const DB_PASSWORD = 'erproot@123';
//    }
}