<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CompanyCustomer;
use Illuminate\Support\Facades\Log;

class CompanyCustomerController extends Controller
{
    //
    public function CheckCompany(Request $request) {
        $data = $request->all();
        Log::debug('company customer: ' . $data['company_name']);
        $where = array();
        if (!empty($data['company_name'])) {
            $where[] = ['com_name', $data['company_name']];
        }

        $companyCustomer = CompanyCustomer::where($where)
            ->first();

        if (empty($companyCustomer)) {
            return json_encode(array('status' => false));
        }

        return json_encode(array('status' => true, 'company_customer' => $companyCustomer));
    }

    public function updateLastLogin(Request $request) {
        $data = $request->all();
        $companyCustomer = CompanyCustomer::where('com_name', $data['company_name'])
            ->first();

        $companyCustomer->last_login_date_at = date('Y-m-d');
        $res = $companyCustomer->save();
        if (empty($res)) {
            return json_encode(array('status' => false));
        }

        return json_encode(array('status' => true));
    }
}
