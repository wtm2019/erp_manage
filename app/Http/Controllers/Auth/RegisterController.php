<?php

namespace App\Http\Controllers\Auth;

use App\CompanyCustomer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $dataJson = json_decode(file_get_contents("php://input"), true);
        $res = $this->ownValidate($dataJson, [
            'com_name'=>'required|unique:company_customers',
            'com_link_man'=>'required',   //后端生成
            'mobile_phone'=>'required|unique:company_customers',
            'com_credit_code'=>'required|unique:company_customers',
            'verify_code'=>'required',
            'com_attr_type'=>'required',
            'pwd'=>'required'
        ]);
        if (!$res['status']) {
            $this->returnData($res['status'], $res['msg']);
        }

        $code = Redis::keys("SMS{$dataJson['mobile_phone']}{$dataJson['verify_code']}");
        if (empty($code)) {
            $this->returnData(false, '短信验证码无效', '');
        }

        $comCode = 111;             //企业代码，需要后台按照一定规则生成
        $userData = array(
            'com_name' => trim($dataJson['com_name']),              //公司名称
            'com_credit_code' => trim($dataJson['com_credit_code']),//统一信用代码
            'com_link_man' => trim($dataJson['com_link_man']),                //联系人
            'mobile_phone' => trim($dataJson['mobile_phone']),      //联系电话
            'pwd' => trim($dataJson['pwd']),                        //登陆密码
            'com_attr_type' => $dataJson['com_attr_type'],          //企业属性：工厂端/销售端
            'com_code' => $comCode,                    //企业代码，后端生成
            'com_login_name' => $dataJson['mobile_phone']          //企业登陆名称使用手机号
        );

        $companyCustomer = CompanyCustomer::create($userData);

        if (!empty($companyCustomer)) {
            $retData = array(
                'id' => $companyCustomer->id
            );

            $this->returnData(true, '注册成功', $retData);
        } else {
            $this->returnData(false, '注册失败', null);
        }
    }
}
