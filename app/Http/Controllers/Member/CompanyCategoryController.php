<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CompanyCategory;
use App\Rules\ExistsInArray;

class CompanyCategoryController extends Controller
{
    //
    private $comTypeAttrMap = array (
        1 => '工厂端',
        2 => '销售端'
    );
    private $statusMap = array (
        1 => '禁用',
        2 => '启用'
    );
    private $statusArray = array(1, 2);

    public function createIndex() {
        $dataJson = json_decode(file_get_contents("php://input"), true);
        $res = $this->ownValidate($dataJson, [
            'com_type_name' => 'required|string|unique:company_categories',
            'com_attr_type' => 'required|integer'
        ]);
        if (!$res['status']) {
            $this->returnData($res['status'], $res['msg']);
        }

        $companyCategory = array(
            'com_type_name' => $dataJson['com_type_name'],
            'com_attr_type' => $dataJson['com_attr_type']
        );

        $res = CompanyCategory::create($companyCategory);
        if (!empty($res)) {
            $this->returnData(true, '新建企业类型成功', $res);
        } else {
            $this->returnData(false, '新建企业类型失败', null);
        }
    }

    public function getIndex(Request $request) {
        $data = $request->all();

        $currentPage = !empty($data['current_page']) ? $data['current_page'] : 1;
        $perPage = !empty($data['per_page']) ? $data['per_page'] : 10;
        $offset = ($currentPage - 1) * $perPage;
        $limit = $perPage;

        $where = array();
        if (!empty($data['com_type_name'])) {
            $where[] = ['com_type_name', 'like', '%' . $data['com_type_name'] . '%'];
        }
        if (!empty($data['com_type_id'])) {
            $where[] = ['com_type_id', $data['com_type_id']];
        }

        $comCategoryList = CompanyCategory::where($where)
            ->select('id', 'com_type_name', 'com_attr_type', 'status')
            ->orderBy('created_at', 'desc')
            ->limit($limit)->offset($offset)
            ->get()->toArray();
        foreach ($comCategoryList as &$comCategory) {
            $comCategory['com_attr_type_name'] = $this->comTypeAttrMap[$comCategory['com_attr_type']];
            $comCategory['status_name'] = $this->statusMap[$comCategory['status']];
        }

        $total = CompanyCategory::where($where)
            ->count();

        $data = array(
            'total' => $total,
            'data' => $comCategoryList
        );

        $this->returnData(true, 'success', $data);
    }

    public function getEnabledIndex(Request $request) {
        $comCategoryList = CompanyCategory::where('status', 2)
            ->select('id', 'com_type_name')
            ->orderBy('created_at', 'desc')
            ->get()->toArray();

        $data = array(
            'data' => $comCategoryList
        );

        $this->returnData(true, 'success', $data);
    }

    public function editIndex() {
        $dataJson = json_decode(file_get_contents("php://input"), true);
        $res = $this->ownValidate($dataJson, [
            'com_type_name' => 'required|string|unique:company_categories,com_type_name,' . $dataJson['com_type_id'],
            'com_type_id' => 'required|integer',
            'com_attr_type' => 'required|integer'
        ]);
        if (!$res['status']) {
            $this->returnData($res['status'], $res['msg']);
        }

        $companyCategory = CompanyCategory::find($dataJson['com_type_id']);
        $companyCategory->com_type_name = $dataJson['com_type_name'];
        $companyCategory->com_attr_type = $dataJson['com_attr_type'];
        $res = $companyCategory->save();
        if ($res) {
            $this->returnData(true, '编辑成功');
        }

        $this->returnData(false, '编辑失败');
    }

    public function changeStatus() {
        $dataJson = json_decode(file_get_contents("php://input"), true);
        $res = $this->ownValidate($dataJson, [
            'status' => ['required', new ExistsInArray($this->statusArray)],
            'com_type_id'=>'required|integer'
        ]);
        if (!$res['status']) {
            $this->returnData($res['status'], $res['msg']);
        }

        $companyCategory = CompanyCategory::find($dataJson['com_type_id']);
        if (empty($companyCategory)) {
            $this->returnData(false, '无此用户');
        }

        $companyCategory->status = $dataJson['status'];

        $res = $companyCategory->save();
        if ($res) {
            $this->returnData(true, $this->statusMap[$dataJson['status']] . '成功');
        }

        $this->returnData(false, $this->statusMap[$dataJson['status']] . '失败');
    }

}
