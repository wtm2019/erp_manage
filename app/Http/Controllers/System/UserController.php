<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Rules\ExistsInArray;

class UserController extends Controller
{
    private $statusMap = array (
        1 => '禁用',
        2 => '启用'
    );

    private $statusArray = array(1, 2);

    public function createIndex(Request $request) {
        $dataJson = json_decode(file_get_contents("php://input"), true);

        $res = $this->ownValidate($dataJson,[
            'user_name'=>'required',
            'user_login_name'=>'required|unique:users',
            'pwd'=>'required'
        ]);
        if (!$res['status']) {
            $this->returnData($res['status'], $res['msg']);
        }

        $password = empty($dataJson['pwd']) ? md5('123456') : $dataJson['pwd'];
        $user = array(
            'user_name' => trim($dataJson['user_name']),
            'user_login_name' => $dataJson['user_login_name'],
            'pwd' => Hash::make($password),
            'status' => empty($dataJson['status']) ? 2 : $dataJson['status'],
        );

        $user = User::create($user);
        if (!empty($user)) {
            $this->returnData(true, '新建用户成功', $user);
        } else {
            $this->returnData(false, '新建用户失败', $user);
        }
    }

    public function getIndex(Request $request) {
        $data = $request->all();
//        $userName = $data['user_name'];
//        $userId = $data['user_id'];
        $currentPage = !empty($data['current_page']) ? $data['current_page'] : 1;
        $perPage = !empty($data['per_page']) ? $data['per_page'] : 10;
        $offset = ($currentPage - 1) * $perPage;
        $limit = $perPage;

        $where = array();
        if (!empty($data['user_name'])) {
            $where[] = ['user_name', 'like', '%' . $data['user_name'] . '%'];
        }
        if (!empty($data['user_id'])) {
            $where[] = ['id', $data['user_id']];
        }

        $userList = User::where($where)
            ->select('id', 'user_name', 'user_login_name', 'status', 'pwd')
            ->orderBy('created_at', 'desc')
            ->limit($limit)->offset($offset)
            ->get()->toArray();
        foreach ($userList as &$userValue) {
            $userValue['status_name'] = $this->statusMap[$userValue['status']];
        }

        $total = User::where($where)
            ->count();

        $data = array(
            'total' => $total,
            'data' => $userList
        );

        $this->returnData(true, 'success', $data);
    }

    public function editIndex() {
        $dataJson = json_decode(file_get_contents("php://input"), true);

        $res = $this->ownValidate($dataJson, [
            'user_name'=>'required',
            'user_id'=>'required',
            'user_login_name'=>'required|unique:users,user_login_name,' . $dataJson['user_id'],
            'pwd'=>'required'
        ]);
        if (!$res['status']) {
            $this->returnData($res['status'], $res['msg']);
        }

        $user = User::find($dataJson['user_id']);
        $user->user_name = $dataJson['user_name'];
        $user->user_login_name = $dataJson['user_login_name'];
        $user->pwd = Hash::make($dataJson['pwd']);
        $res = $user->save();
        if ($res) {
            $this->returnData(true, '编辑成功');
        }

        $this->returnData(false, '编辑失败');
    }

    public function changeStatus() {
        $dataJson = json_decode(file_get_contents("php://input"), true);
        $res = $this->ownValidate($dataJson, [
            'status' => ['required', new ExistsInArray($this->statusArray)],
            'user_id'=>'required|integer'
        ]);
        if (!$res['status']) {
            $this->returnData($res['status'], $res['msg']);
        }

        $user = User::find($dataJson['user_id']);
        if (empty($user)) {
            $this->returnData(false, '无此用户');
        }

        $user->status = $dataJson['status'];

        $res = $user->save();
        if ($res) {
            $this->returnData(true, $this->statusMap[$dataJson['status']] . '成功');
        }

        $this->returnData(false, $this->statusMap[$dataJson['status']] . '失败');
    }}
