<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\Constants;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userInfo = unserialize(Redis::get($_COOKIE['_token']));
//        登录失效
        if (!(intval($userInfo['user_id']) >= 1)) {
            Redis::del($_COOKIE['_token']);
            echo json_encode(array(
                'retCode' => 5,
                'retMsg' => '登录失效',
                'retData' => ''
            ));
            exit;
        }

        Redis::expire(serialize($userInfo), Constants::LOGIN_REDIS_EXPIRE);
        //跳到操作
        return $next($request);
    }
}
